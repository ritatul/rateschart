import React, { Component } from 'react';
import './App.css';
import RatesChart from './components/ratesChart';

class App extends Component {
  render() {
    return (
      <div className="App">
        <RatesChart />
      </div>
    );
  }
}

export default App;
