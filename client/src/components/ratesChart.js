import React, { Component } from 'react';
import './ratesChart.css';
import Chart from 'react-google-charts';
import _ from 'underscore';
import { Rnd } from "react-rnd";

const style = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  border: 'solid 2px #ddd'
};

class RatesChart extends Component {
  constructor() {
    super();
    let locationData = JSON.parse(localStorage.getItem("location"));
    this.state = {
      data: [],
      ticks:[],
      width: locationData && locationData.width ? locationData.width : 700,
      height: locationData && locationData.height ? locationData.height : 400,
      x: locationData && !_.isUndefined(locationData.x) ? locationData.x : 50,
      y: locationData && !_.isUndefined(locationData.y) ? locationData.y : 50
    };
    this.updateDimensions = this.updateDimensions.bind(this);
  }

  componentDidMount() {
      this.fetchData();
      window.onbeforeunload = (e) => {
        localStorage.setItem("location",JSON.stringify({x:this.state.x ,y:this.state.y, width:this.state.width, height:this.state.height}));
      };
      window.addEventListener("resize", this.updateDimensions);
  }

  fetchData() {
    fetch('/chartdata')
      .then(res => res.json())
      .then(result => {
        let group = _.groupBy(result,'date');
        let keys = Object.keys(group);
        let vals = Object.values(group);
        let ticks = [];
        let data = vals.map((val,i) => {
          let rates = {};
          val.forEach((item)=>{
              rates[item.name] = item.rate;
          });
          ticks.push(new Date(keys[i]));
          return [new Date(keys[i]),rates['EUR/USD'],rates['EUR/BTC'],rates['EUR/ILS']];
        });
        data.reverse();
        data.unshift([{type:'date'}, 'EUR/USD', 'EUR/BTC', 'EUR/ILS']);
        this.setState({data, ticks})
        });
  }

  updateDimensions() {
    let windowWidth = typeof window !== "undefined" ? window.innerWidth : 0;
    let windowHeight = typeof window !== "undefined" ? window.innerHeight : 0;
    let width =  parseInt(this.state.width, 10) > windowWidth ? windowWidth-50+'px' : this.state.width;
    let height =  parseInt(this.state.height, 10) > windowHeight ? windowHeight-50+'px' : this.state.height;
    let x = parseInt(this.state.width, 10) + this.state.x > windowWidth ? window.innerWidth-parseInt(this.state.width, 10)-2 : this.state.x;
    let y = parseInt(this.state.height, 10) + this.state.y > windowHeight ? window.innerHeight-parseInt(this.state.height, 10) : this.state.y;
    this.setState({width , height, x, y});
  }

  render() {
    return (
      <div className="container">
        <Rnd
          style={style}
          size={{ width: this.state.width, height: this.state.height}}
          position={{ x: this.state.x, y: this.state.y }}
          onDragStop={(e, d) => {
            d.x = d.x < 0 ? 0 : d.x + parseInt(this.state.width, 10) > window.innerWidth ? window.innerWidth-parseInt(this.state.width, 10)-2 : d.x;
            d.y = d.y < 0 ? 0 : d.y + parseInt(this.state.height, 10) > window.innerHeight ? window.innerHeight-parseInt(this.state.height, 10) : d.y;
            this.setState({ x: d.x, y: d.y });
          }}
          onResizeStop={(e, direction, ref, delta, position) => {
            ref.style.width = parseInt(ref.style.width,10) > window.innerWidth ? window.innerWidth - 10 +'px' : ref.style.width;
            ref.style.height = parseInt(ref.style.height,10) > window.innerHeight ? window.innerHeight - 10 +'px' : ref.style.height; 
            this.setState({
              width: ref.style.width,
              height: ref.style.height,
              ...position
            });
          }}
        >
         {this.state.data.length > 0 && <Chart
            width='100%'
            height='100%'
            chartType="LineChart"
            loader={<div>Loading Chart</div>}
            data={this.state.data}
            options={{
                chartArea: {width: '90%', height: '70%'},
                intervals: { style: 'sticks' },
                legend: { position: 'bottom' },
                selectionMode: 'multiple',
                tooltip: {trigger: 'both'},
                aggregationTarget: 'none',
                focusTarget: 'category',
                crosshair: {
                    trigger: 'both',
                    orientation: 'vertical'
                },
                hAxis: {
                  format: 'Y-MM-d',
                  ticks: this.state.ticks
                }
            }}
            formatters={[{
              type: 'DateFormat',
              column: 0,
              options: {
                pattern: 'Y-MM-d'
              }
            }]}
          />}
      </Rnd>
      </div>
    );
  }
}

export default RatesChart;
