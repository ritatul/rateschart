const mysql = require('mysql');

const con = mysql.createConnection({
    host: "63.33.172.234",
    port: "3554",
    user: "guest",
    password: "GHE3FJU"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("DB Connected!");
});

const getData = () => {
    return new Promise((resolve, reject) => {
        let sqlQuery = `SELECT rate,DATE_FORMAT(time_created,"%Y/%m/%d") date,name FROM tradair.rates
                    JOIN tradair.currency_pairs ON tradair.rates.currency_pair_id = tradair.currency_pairs.id
                    ORDER BY time_created DESC limit 21`;
        con.query(sqlQuery, (err, rows) => {
            if (err) reject(err);
            resolve(rows);
        });
    });
};

module.exports = {
    getData: getData
};