var express = require('express'),
    router = express.Router(),
    db = require('../models/dbMethods');

router.get('/chartdata', (req, res) => {
    db.getData()
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.status(500).json({ success: false, msg: `Something went wrong. ${err}` });
      });
  });

module.exports = router;
