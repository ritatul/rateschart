const express = require('express');
const charts = require('./routes/charts');
const app = express();

app.use('/', charts);
const port = 5000;

app.listen(port, () => `Server running on port ${port}`);